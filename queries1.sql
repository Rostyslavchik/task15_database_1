USE labor_sql;

#1.1
/*
SELECT maker FROM product, laptop 
WHERE laptop.model = product.model;

SELECT maker FROM product
JOIN laptop ON (laptop.model = product.model);
*/
#SELECT maker, type FROM product WHERE type='Laptop' ORDER BY maker;
#1.7
#SELECT * FROM printer WHERE type != 'Matrix' AND price < 300 ORDER BY type DESC;
#1.14
#SELECT model, speed, price FROM pc WHERE speed BETWEEN 500 AND 700;

#1.5
SELECT `name`, `class` FROM ships WHERE `name` = `class` ORDER BY name; 
#1.19
SELECT * FROM classes WHERE country = 'Japan' ORDER BY `type` DESC;
#1.23
SELECT class, displacement FROM classes WHERE displacement >= 40000 ORDER BY `type`;

#2.1
#SELECT model FROM pc WHERE model LIKE '%1%1%';

#2.4
SELECT `name` FROM ships WHERE `name` LIKE 'W%n';
#2.5
SELECT `name` FROM ships WHERE `name` RLIKE '.*e.*e.*';
#2.6
SELECT `name`, launched FROM ships WHERE `name` RLIKE '^.*[^a]$';
#2.7
SELECT `name` FROM battles WHERE `name` RLIKE '^.+ .+[^c]$';

#3.1
#SELECT maker, `type`, speed, hd FROM pc 
#JOIN product ON (pc.model = product.model) 
#WHERE hd <= 8;
#3.6
#SELECT pc.model, maker FROM pc
#JOIN product ON (pc.model = product.model)
#WHERE price < 600;
#3.10
#SELECT maker, `type`, laptop.model, speed FROM laptop
#JOIN product ON (laptop.model = product.model)
#WHERE speed > 600;

#3.5
SELECT c2.country, c1.class, c2.class FROM classes c1 
JOIN classes c2 ON (c1.`type` = 'bb' AND c2.`type` = 'bc' AND c1.country = c2.country); 
#3.11
SELECT s.*, c.displacement FROM ships s
JOIN classes c ON (s.class = c.class);
#3.13
SELECT s.*, c.country FROM ships s 
JOIN classes c ON (s.class = c.class);