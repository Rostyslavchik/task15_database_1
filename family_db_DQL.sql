USE family;

SELECT * FROM sex;
SELECT * FROM family_member;
SELECT * FROM family_tree_member;
SELECT * FROM family_value;
SELECT * FROM family_value_family_tree_member;

SELECT family_value.id as value_id, family_value.name as value_name, family_value.approximate_price as a_price, family_tree_member.id as member_id, 
family_tree_member.name as member_name, family_tree_member.surname as member_surname, family_tree_member.birthday as member_birthday
FROM family_value 
JOIN family_value_family_tree_member ON (family_value.id = family_value_family_tree_member.family_value_id)
JOIN family_tree_member ON (family_tree_member.id = family_value_family_tree_member.family_tree_member_id); # JOIN for many-to-many


SELECT family_value.id as value_id, family_value.name as value_name, family_value.approximate_price as a_price, family_tree_member.id as member_id, 
family_tree_member.name as member_name, family_tree_member.surname as member_surname, family_tree_member.birthday as member_birthday
FROM family_tree_member 
JOIN family_value_family_tree_member ON (family_tree_member.id = family_value_family_tree_member.family_tree_member_id) # JOIN for many-to-many
JOIN family_value ON (family_value.id = family_value_family_tree_member.family_value_id);

SELECT * FROM family_member
JOIN sex ON (family_member.sex_id = sex.id); # JOIN for one-to-many
/*
SELECT f.surname, f.name, f.birthday, s.sex FROM family_member f, sex s
WHERE f.sex_id = s.id;
*/
SELECT * FROM family_member, sex
WHERE family_member.sex_id = sex.id; # the same as JOIN one-to-many

SELECT * FROM family_tree_member
JOIN family_member ON (family_tree_member.family_member_id = family_member.id);

SELECT f1.surname as f1_surname, f1.name as f1_name, f2.surname as f2_surname, f2.name as f2_name
FROM family_tree_member f1
JOIN family_tree_member f2 ON (f1.family_tree_member_id = f2.id) # join for recursive relationship one-to-many

