DROP DATABASE IF EXISTS family;
CREATE DATABASE family;
USE family;

CREATE TABLE sex(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
sex VARCHAR(20) NOT NULL
);

CREATE TABLE family_member(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
surname VARCHAR(30) NOT NULL,
name VARCHAR(30) NOT NULL,
birthday DATE NOT NULL,
death_date DATE,
birth_place POINT NOT NULL,
death_place POINT,
mariage_date DATE,
sex_id INT,
fullinfo VARCHAR(60) AS(CONCAT(name,' ', surname, ' was born: ', DAY(birthday), 'th ', YEAR(birthday), ' A. D.')),
INDEX death_birthday_indx(birthday, death_date),
FOREIGN KEY (sex_id) REFERENCES sex(id) ON DELETE SET NULL ON UPDATE SET NULL #я використав НеОбов’язковий неідентифікуючий зв’язок
);

CREATE TABLE family_tree_member(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
family_tree_member_id INT, #null is allowed
surname VARCHAR(30) NOT NULL,
name VARCHAR(30) NOT NULL,
fullname VARCHAR(60) AS(CONCAT(surname, name)),
birthday DATE NOT NULL,
death_date DATE,
birth_place POINT NOT NULL,
death_place POINT,
credit_card_number INT,
family_member_id INT UNIQUE KEY,
sex_id INT,
#FOREIGN KEY (family_member_id) REFERENCES family_member(id) ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY (family_member_id) REFERENCES family_member(id) ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY (sex_id) REFERENCES sex(id) ON DELETE SET NULL ON UPDATE CASCADE /*я використав НЕОбов’язковий неідентифікуючий зв’язок*/,
#FOREIGN KEY (family_tree_member_id) REFERENCES family_tree_member(id) ON DELETE RESTRICT ON UPDATE RESTRICT #Рекурсивний зв'язок 1:M(це зв'язок у вигляді дерева)
FOREIGN KEY (family_tree_member_id) REFERENCES family_tree_member(id) ON DELETE RESTRICT  ON UPDATE CASCADE
);

CREATE TABLE family_value(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(30) NOT NULL,
approximate_price FLOAT NOT NULL,
max_price FLOAT NOT NULL,
min_price FLOAT NOT NULL,
catalogue_code INT NOT NULL UNIQUE,
factor DOUBLE AS(SIN(min_price) + COS(max_price)),
INDEX max_price_indx(max_price)
);

CREATE TABLE family_value_family_tree_member(
family_value_id INT NOT NULL,
family_tree_member_id INT NOT NULL,
PRIMARY KEY(family_value_id, family_tree_member_id),
#FOREIGN KEY(family_value_id) REFERENCES family_value(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY(family_value_id) REFERENCES family_value(id) ON DELETE RESTRICT  ON UPDATE CASCADE,
#FOREIGN KEY(family_tree_member_id) REFERENCES family_tree_member(id) ON DELETE RESTRICT ON UPDATE RESTRICT
FOREIGN KEY(family_tree_member_id) REFERENCES family_tree_member(id) ON DELETE RESTRICT  ON UPDATE CASCADE
);

 