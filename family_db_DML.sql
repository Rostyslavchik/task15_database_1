USE family;

/*
INSERT INTO sex(sex) VALUES('male');
INSERT INTO sex(sex) VALUES('female');

INSERT INTO family_member(surname, name, birthday, death_date, birth_place, death_place, mariage_date, sex_id) 
VALUES('John', 'Kripke', '1892-05-06', '1834-04-15', POINT(34.545334534, 56.46453635), POINT(675.545334594, 536.46483635), 
'1910-11-25', 1);

INSERT INTO family_member(surname, name, birthday, death_date, birth_place, death_place, mariage_date, sex_id) 
VALUES
#('Anna', 'Filipovic', '1895-04-12', '1832-11-16', POINT(34.542334534, 56.46453635), POINT(675.543434594, 536.46563635), '1910-11-25', 2),
('Tom', 'Tompson', '1911-01-13', '1859-06-16', POINT(33.545334534, 55.46453635), POINT(645.545334594, 576.46483635), '1929-10-23', 1),
('Joanna', 'Jat', '1913-02-05', '1969-05-15', POINT(34.545334534, 56.46453635), POINT(67.545334594, 5.46483635), '1928-11-25', 2),
('Anna', 'Filipovic', '1915-04-12', '1970-11-16', POINT(34.542334534, 56.46453635), POINT(675.543434594, 536.46563635), '1936-11-2', 2),
('Johny', 'Karlson', '1935-07-30', '1994-02-1', POINT(3874.6765334534, 4654.48675635), POINT(534.545334594, 56.46483635), '1958-1-5', 1);


INSERT INTO family_tree_member(family_tree_member_id, surname, name, birthday, death_date, birth_place, death_place, 
credit_card_number, family_member_id, sex_id)
VALUES
(NULL, 'Josephina', 'Relon', '1895-07-5', '1940-5-16', POINT(33.545334534, 55.46453635), POINT(645.545334594, 576.46483635), 333333, 1, 2),
(1, 'Antonia', 'Bachmack', '1913-5-4', '1957-07-30', POINT(33.545334534, 55.46453635), POINT(645.545334594, 576.46483635), 22222222, 2, 2),
(1, 'Jack', 'Stalone', '1914-07-4', '1937-5-16', POINT(33.545634534, 55.46483635), POINT(0.545334594, 0.46483635), 1111111, 3, 1),
(1, 'Ben', 'Rikite', '1912-06-1', '1978-5-6', POINT(3.54534534, 5.46453635), POINT(645.545376594, 576.46465635), 000000, 4, 1),
(3, 'Lilly', 'Nelson', '1937-07-5', '1996-3-16', POINT(33.545334534, 55.4645365), POINT(645.54533454, 576.46483635), 333333, 5, 2),
# The next ones are leafs 
(5, 'Sam', 'Winchester', '1985-07-4', '2002-5-16', POINT(3.545334534, 5.46453635), POINT(45.545334594, 5.7646483635), 999999, NULL, 1),
(4, 'Dean', 'Winchester', '1950-07-7', NULL, POINT(33.545334534, 55.46453635), NULL, 888888, NULL, 1),
(2, 'Casstiel', 'Archangel', '1940-07-18', NULL, POINT(0.0000000, 0.0000000), NULL, 777777, NULL, 1);

INSERT INTO family_value(name, approximate_price, max_price, min_price, catalogue_code) 
VALUES
('Golden Apple', 2500.54, 10000.34, 100, 345535),
('Silver Blade', 1345.5, 5678.4, 10, 58684),
('Copper hairpin', 2990.67, 12000.68, 200, 3879654);

INSERT INTO family_value_family_tree_member(family_value_id, family_tree_member_id) 
VALUES
(1, 1),
(2, 1),
(3, 1),
(1, 2),
(3, 2),
(2, 3),
(3, 8),
(3, 7);
*/

#DELETE FROM family_tree_member WHERE family_member_id = 1;
#UPDATE family_member SET family_member.id = 3 WHERE family_member.id = 1;
#DELETE FROM sex WHERE sex.id = 1;
UPDATE sex SET sex.id = 3 WHERE sex.id = 1;